#!/bin/sh
# Launching a local (non-containerized) version of tide_test app
# for faster development cycle.
# Make sure you have zookeeper and kafka up and running when
# you execute this one.

set -x

export SIMPLE_SETTINGS=settings
export WORKER=tide_test
export WORKER_PORT=6066
export KAFKA_BOOTSTRAP_SERVER='kafka://kafka:9092'
export KAFKA_BOOSTRAP_SERVER_NAME=kafka
export KAFKA_BOOSTRAP_SERVER_PORT=9092

./wait_for_services.sh
$WORKER worker --web-port=$WORKER_PORT
