from setuptools import setup, find_packages

requires = [
    'colorlog>=3.1.4',
    'faust @ git+https://github.com/robinhood/faust.git@bfdd38a9#egg=faust',
    'robinhood-aiokafka==1.1.3',
    'simple-settings>=0.16.0'
]

setup(
    name='tide_test',
    version='0.2.1',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    entry_points={
        'console_scripts': [
            'tide_test = tide_test.app:main',
        ]
    },
)
