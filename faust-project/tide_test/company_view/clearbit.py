"""
Clearbit API related stuff
See https://clearbit.com/docs
"""

import os
import datetime as dt
from copy import deepcopy
from types import SimpleNamespace

import aiohttp
# from simple_settings import settings



class AbnormalCompanyInfo(Exception):
    """Raised when available company info does not allow to determine
    one or more of the essential attributes: name, domain, age, employees.
    """


class CompanyInfoWrapper:
    """Wrapper around company info returned by clearbit api."""

    def __init__(self, info: dict) -> None:
        self._info = info
        self._yearnow = dt.datetime.now().year

    @property
    def name(self) -> str:
        try:
            name = self._info['name']
        except KeyError as err:
            raise AbnormalCompanyInfo(f"{err} key not found in company_info={self._info!r}")

        if not name:
            try:
                name = self._info['domain'].split('.')[-2]
            except KeyError as err:
                raise AbnormalCompanyInfo(f"{err} key not found in company_info={self._info!r}")
            except IndexError:
                msg = f"Cannot derive company name from domain for company_info={self._info!r}"
                raise AbnormalCompanyInfo(msg)

        return name

    @property
    def domain(self) -> str:
        try:
            return self._info['domain']
        except KeyError as err:
            raise AbnormalCompanyInfo(f"{err} key not found in company_info={self._info!r}")

    @property
    def age(self) -> int:
        try:
            return self._yearnow - int(self._info['foundedYear'])

        except KeyError as err:
            raise AbnormalCompanyInfo(f"{err} key not found in company_info={self._info!r}")

        except (ValueError, TypeError):
            msg = f"Cannot convert to int: foundedYear={self._info['foundedYear']!r}"
            raise AbnormalCompanyInfo(msg)

    @property
    def employees(self) -> int:
        try:
            return self._info['metrics']['employees']
        except KeyError as err:
            raise AbnormalCompanyInfo(f"{err} key not found in company_info={self._info!r}")


class ClearbitApi:
    """The Clearbit API (currently implementing a single Company API call).
    (See https://dashboard.clearbit.com/docs#enrichment-api-company-api-domain-lookup)
    Expects an environment variable CLEARBIT_SKEY to contain the API secret key.
    """

    CLEARBIT_COMPANY_FIND_BASE_URL = 'https://company.clearbit.com/v2/companies/find'
    CLEARBIT_SKEY = os.environ['CLEARBIT_SKEY']

    async def company_by_domain(self, domain):
        base_url = self.CLEARBIT_COMPANY_FIND_BASE_URL
        full_url = f"{base_url}?domain={domain}"
        auth = aiohttp.BasicAuth(self.CLEARBIT_SKEY)

        async with aiohttp.ClientSession(auth=auth) as session:
            async with session.get(full_url) as response:
                body = await response.json()
                return SimpleNamespace(
                    body=body, status=response.status, reason=response.reason)


import unittest

class TestCompanyInfoWrapper(unittest.TestCase):

    sample_info = {
        "id": "3f5d6a4e-c284-4f78-bfdf-7669b45af907",
        "name": "Uber",
        "legalName": "Uber Technologies Inc.",
        "domain": "uber.com",
        "site": {
            "phoneNumbers": [],
            "emailAddresses": []
        },
        "category": {
            "sector": "Information Technology",
            "industryGroup": "Software & Services",
            "industry": "Internet Software & Services",
            "subIndustry": "Internet",
            "sicCode": "87",
            "naicsCode": "51"
        },
        "description": "Thanks for visiting the Uber Facebook page! By using or accessing our Facebook page, you agree to comply with Facebook's Statement of Rights and Responsibilities. As always, the content posted by fans of Uber's Facebook page does not reflect Uber’s vie...",
        "foundedYear": 2009,
        "location": "1455 Market St, San Francisco, CA 94103, USA",
        "timeZone": "America/Los_Angeles",
        "utcOffset": -8,
        "logo": "https://logo.clearbit.com/uber.com",
        "emailProvider": False,
        "type": "public",
        "ticker": "UBER",
        "identifiers": {
            "usEIN": "452647441"
        },
        "phone": "+1 415-986-2104",
        "metrics": {
            "alexaUsRank": 476,
            "alexaGlobalRank": 770,
            "employees": 22260,
            "employeesRange": "10K-50K",
            "marketCap": None,
            "raised": None,
            "annualRevenue": 11270000000,
            "estimatedAnnualRevenue": "$10B+",
            "fiscalYearEnd": 12
        },
        "indexedAt": "2019-11-01T04:19:50.445Z",
    }

    def test_creation_default_fails(self):
        with self.assertRaises(TypeError):
            CompanyInfoWrapper()

    def test_creation_with_info_ok(self):
        CompanyInfoWrapper(self.sample_info)

    def test_age__ok(self):
        cinfo = CompanyInfoWrapper(self.sample_info)
        self.assertEqual(10, cinfo.age)

    def test_age__key_not_found(self):
        info = deepcopy(self.sample_info)
        del info['foundedYear']
        cinfo = CompanyInfoWrapper(info)
        with self.assertRaises(AbnormalCompanyInfo):
            cinfo.age

    def test_age__non_int_value(self):
        info = deepcopy(self.sample_info)
        info['foundedYear'] = None
        cinfo = CompanyInfoWrapper(info)
        with self.assertRaises(AbnormalCompanyInfo):
            cinfo.age

    def test_name__ok(self):
        cinfo = CompanyInfoWrapper(self.sample_info)
        self.assertEqual('Uber', cinfo.name)

    def test_name__from_domain(self):
        info = deepcopy(self.sample_info)
        info['name'] = None
        cinfo = CompanyInfoWrapper(info)
        self.assertEqual('uber', cinfo.name)

    def test_name__from_domain__index_error(self):
        info = deepcopy(self.sample_info)
        info['name'] = None
        info['domain'] = 'notldhere'
        cinfo = CompanyInfoWrapper(info)
        with self.assertRaises(AbnormalCompanyInfo):
            cinfo.name

    def test_domain__ok(self):
        cinfo = CompanyInfoWrapper(self.sample_info)
        self.assertEqual('uber.com', cinfo.domain)

    def test_domain__key_not_found(self):
        info = deepcopy(self.sample_info)
        del info['domain']
        cinfo = CompanyInfoWrapper(info)
        with self.assertRaises(AbnormalCompanyInfo):
            cinfo.domain

    def test_employees__ok(self):
        cinfo = CompanyInfoWrapper(self.sample_info)
        self.assertEqual(22260, cinfo.employees)

    def test_employees__key_not_found(self):
        # The only case here where a nested dict is manipulated
        # so deepcopy() is absolutely necessary:
        info = deepcopy(self.sample_info)

        del info['metrics']['employees']
        cinfo = CompanyInfoWrapper(info)
        with self.assertRaises(AbnormalCompanyInfo):
            cinfo.employees

        del info['metrics']
        cinfo = CompanyInfoWrapper(info)
        with self.assertRaises(AbnormalCompanyInfo):
            cinfo.employees


if __name__ == "__main__":
    unittest.main()
