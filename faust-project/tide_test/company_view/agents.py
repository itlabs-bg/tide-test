
import logging
import aiohttp
import random
from types import SimpleNamespace

from faust.web import Request, Response, View
from faust.web.exceptions import NotFound
from simple_settings import settings

from tide_test.app import app
from tide_test.company_view.models import Company, CompanyDetails
from tide_test.company_view.clearbit import ClearbitApi, CompanyInfoWrapper, AbnormalCompanyInfo

log = logging.getLogger(__name__)

# Using anything else but `partitions=1` here creates an issue
# discussed in https://github.com/robinhood/faust/issues/358
companies_topic = app.topic('companies', partitions=1, value_type=Company)
domain_info_topic = app.topic('domain_info', partitions=1, value_type=str)
company_details_tbl = app.Table('company_details', partitions=1, key_type=str,
                                value_type=CompanyDetails)
clearbit_api = ClearbitApi()

messages = [] # use as in-memory data-base will be wiped on restart


def error_dict(message='Unknown error'):
    return dict(status='error', message=message)


@app.agent(companies_topic)
async def store_company_views(companies):
    async for company in companies:
        log.info("Received message for company %s", company.name)
        messages.append(company)
        response = await clearbit_api.company_by_domain(company.domain)
        log.debug("Received clearbit response status=%r %s, body=%r",
                  response.status, response.reason, response.body)

        if response.status != 200:
            log.warning("Non-200 response from clearbit - skip processing")
        else:
            await domain_info_topic.send(value=response.body)
            log.info("Sending  body for domain %s to topic domain_info", company.domain)


@app.agent(domain_info_topic)
async def store_company_info(domain_info):
    async for company_info in domain_info:
        wrapped_info = CompanyInfoWrapper(company_info)
        try:
            name = wrapped_info.name
            domain = wrapped_info.domain
            age = wrapped_info.age
            employees = wrapped_info.employees

        except AbnormalCompanyInfo as err:
            log.error("AbnormalCompanyInfo: %s - disregarding company info", err)
            return

        company_details_tbl[name.lower()] = CompanyDetails(name, domain, age, employees)
        log.info("Company details read from 'domain_info_topic' stored into 'company_details_tbl':"
                 "name=%r, domain=%r, age=%r, employees=%r", name, domain, age, employees)


@app.page('/companies/')
class counter(View):
    async def get(self, request):
        log.info(f"GET /companies/; messages={messages}")
        return self.json({'messages': messages})

    async def post(self, request):
        body = await request.json()
        await companies_topic.send(value=body)
        log.info("Request processed - body=%r", body)
        return self.json({'processed': True})


@app.page('/companies/{name}')
class company_details(View):
    async def get(self, request, name):
        log.info("GET /companies/%s", name)
        try:
            result = company_details_tbl[name.lower()].as_dict()
        except KeyError:
            log.warning("404 Company name %r NOT found", name)
            raise NotFound('Company name not found')
        else:
            log.info("200 OK")
            return self.json(result)
