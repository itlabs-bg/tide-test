import faust


class Company(faust.Record):
    name: str
    domain: str


class CompanyDetails(faust.Record):
    name: str
    domain: str
    age: int
    employees: int

    def as_dict(self):
        return self.__dict__.copy()


import unittest

class TestCompanyDetails(unittest.TestCase):

    def test_creation_default_fails(self):
        with self.assertRaises(TypeError):
            CompanyDetails()

    def test_creation_4_args_ok(self):
        cd = CompanyDetails('thename', 'thedomain.tld', 42, 12345)
        self.assertEqual('thename', cd.name)
        self.assertEqual('thedomain.tld', cd.domain)
        self.assertEqual(42, cd.age)
        self.assertEqual(12345, cd.employees)

    def test_as_dict(self):
        cd = CompanyDetails('name2', 'domain2.tld', 2, 2222)
        expected = {
            'name': 'name2',
            'domain': 'domain2.tld',
            'age': 2,
            'employees': 2222,
        }
        self.assertEqual(expected, cd.as_dict())

    def test_as_dict__returns_copy(self):
        cd = CompanyDetails('name3', 'domain3.tld', 33, 3333)
        self.assertIsNot(cd.as_dict(), cd.as_dict())


if __name__ == "__main__":
    unittest.main()
