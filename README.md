# tide-test

tide-test assignment solution
by YD, 2019-11-25

## Where's the assignment?

It used to be here in the `README.md` but has been moved to `ASSIGNMENT.md`.

## Prerequisites

- Docker (known to work with version 18.09.9)
- docker-compose (known to work with version 1.24.1)
- Entry for `kafka` hostname in `/etc/hosts` pointing to `127.0.0.1`
  (localhost), e.g.:

```
# /etc/hosts fragment
127.0.0.1  localhost  kafka
```

- git (the one used here is version 2.17.1)
- A secret key for `Clearbit` API to be obtained and set into an environment
  variable, like:

```bash
export CLEARBIT_SKEY=sk_123456789abcdef999888777
```

## Notable changes

- ### Number of partitions

I've set the number of partitions of the two Kafka topics and the table changelog
topic to 1. The reason is if we use something else, the `domain_info` topic ends
with a single partition, as it is created by the Kafka server on first use due to
this setting:
`auto.create.topics.enable = true`
and another one determining the number of partitions for auto-created topics:
`num.partitions = 1`

This way its partitions number differs from the one of `1-company_details-changelog`
which brings this error:
```
faust.exceptions.PartitionsMismatch: The source topic 'domain_info' for table 'company_details'
has 1 partitions, but the changelog
topic '1-company_details-changelog' has 4 partitions.

Please make sure the topics have the same number of partitions
by configuring Kafka correctly.
```

This problem is described also in [issue 325](https://github.com/robinhood/faust/issues/325) and [issue 358](https://github.com/robinhood/faust/issues/358).

- ### faust version

The `faust` version used is git repo master `bfdd38a9` (see [the github commits thread](https://github.com/robinhood/faust/commits/master)). The particular bug that no other versions (not even `1.9.0`) solves for me is [issue 450](https://github.com/robinhood/faust/issues/450) and the possibly related [issue 458](https://github.com/robinhood/faust/issues/458).

To get this working, `setup.py` now references `faust` via
```
   'faust @ git+https://github.com/robinhood/faust.git@bfdd38a9#egg=faust'
```
This required `robinhood-aiokafka` version to be bumped to `1.1.3`.

- ### Makefile

Slight improvements, specifically in the `clean` target

- ### Dockerfile

Added installing `git`  into the `faust-project` image allowing the git-based installation of `faust`.

## Run the app

Please make sure you have met all the prerequisites above.

### 1. Clone the repo
```
$ git clone https://bitbucket.org/itlabs-bg/tide-test.git
```
### 2. Build the images:
```
# last chance to set the api key if you didn't already ;)
$ export CLEARBIT_SKEY=sk_123456789abcdef999888777
$ cd tide-test
$ make build
```

### 3. Run it
```
$ make run
```

### 4. Try it

Retrieving `/companies/` yields an empty list for now:
```
curl "http://localhost:6066/companies/"
```
and any attempt to specify a particular company returns 404:
```
curl -w " %{http_code}\n" "http://localhost:6066/companies/ibm"
```
The response is -> `{"error": "Company name not found"} 404`

Now let's do a POST with a company like IBM:
```
curl "http:///localhost:6066/companies/" \
  -X POST \
  -d '{"name":"IBM","domain":"ibm.com"}' \
  -H "Content-Type: application/json"
```

Now `curl "http://localhost:6066/companies/ibm"` yields:
`{"name": "IBM", "domain": "ibm.com", "age": 108, "employees": 380000}`

Meanwhile, the `curl "http://localhost:6066/companies/"` request brings
```
{
  "messages": [
    {
      "name": "IBM",
      "domain": "ibm.com",
      "__faust": {
        "ns": "tide_test.company_view.models.Company"
      }
    }
  ]
}
```
.
Hope this works fine for you...
### Enjoy! ;)
