service=faust-project
worker=tide_test.app
partitions=4


# Build docker image
build:
	docker-compose build

run:
	docker-compose up

logs:
	docker-compose logs

# Removes old containers, free's up some space
remove: stop
	# Try this if this fails: docker rm -f $(docker ps -a -q)
	# docker-compose rm --force -v
	docker ps -a | awk '/confluentinc\/cp-(kafka|zookeeper)/ {print $$1}' | xargs docker rm \
	  || echo "*** NO-CONTAINERS-TO-REMOVE"

remove-network:
	docker network rm tide-test_default || echo "*** NO-NETWORK-TO-REMOVE"

stop:
	docker-compose stop

run-dev: build run

clean: remove remove-network
